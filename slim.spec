%define debug_package %{nil}

Name:           slim
Version:        1.40.11
Release:        1%{?dist}
Summary:        "Don't change anything in your Docker container image and minify it by up to 30x (and for compiled languages even more) making it secure too!"
Group:          Applications/System
License:        ASL 2.0
URL:            https://github.com/slimtoolkit/slim
Source0:        https://downloads.dockerslim.com/releases/%{version}/dist_linux.tar.gz

%description
"Don't change anything in your Docker container image and minify it by up to 30x (and for compiled languages even more) making it secure too!"

%global debug_package %{nil}

%prep
%autosetup -n %{name} -c

%install
ls -laish
install -d -m 755 %{buildroot}%{_bindir}
install -p -m 755 dist_linux/%{name} %{buildroot}%{_bindir}/%{name}
install -p -m 755 dist_linux/slim-sensor %{buildroot}%{_bindir}/%{name}-sensor

%files
%{_bindir}/%{name}
%{_bindir}/%{name}-sensor

%changelog
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.11

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.7

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.6

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.4

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.3

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.2

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.1

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.40.0

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.39.0

* Sat Sep 17 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
